/*Display array*/

fetch('users.json')
.then(function(response) {
    return response.json();
}) .then(function(data){
    console.log (data['customers']);
}).catch(function(error){
    console.error(error);
})

/*Retrieve users name & title*/

fetch('users.json').then(response => {
  response.json().then((data) => {
    let dataTab = data['customers']
    dataTab.forEach(user => {
      console.log(user['user_title']+' '+user['user_name']);
    })
  })
})

/*Retrieve animals name*/

fetch('users.json').then(response => {
  response.json().then((data) => {
    let dataTab = data['customers'];
    let animalsName = [];

    dataTab.forEach(user => {
        user['user_pets'].forEach(pet => {
          animalsName.push(pet['pet_name']);
          animalsName.sort()
        })
    })
    console.log(animalsName.toString());
  })
})

/*Retrieve only users who own at least 1 animal*/

fetch('users.json').then((response) => {
  response.json().then((data) => {
    let dataTab = data['customers']

    dataTab.forEach(user => {
      if(user['user_pets'].length>0){
        console.log(user['user_name'])};
    })
  })
})

/*Addition of an animal for each user and display users list*/

fetch('users.json').then((response) => {
  response.json().then((data) => {
    let dataTab = data['customers']
    let mickey = { pet_name : 'Mickey', pet_type : 'mouse', pet_age : 0.1 }
    
    dataTab.forEach(user => {
      user['user_pets'].push(mickey);
        console.log(user['user_title']+' '+user['user_name'])
      user['user_pets'].forEach(pet =>
        console.log(pet['pet_name']+' is a '+pet['pet_type']+" and is "+pet['pet_age'])
      )})
  })
})
